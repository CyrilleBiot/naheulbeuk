#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# dépendance python3-vlc

from subprocess import call

import os, random, vlc, time

# On met le son à fond ;)
call(["amixer", "-D", "pulse", "sset", "Master", "100%"])

# Lister les mp3
path = './mp3/'
files = os.listdir(path)

# Boucle infinie, on les lit
while True:
    # On en prend un au hasard
    file_to_play = random.choice(files)
    file_to_play = path + file_to_play
    print(file_to_play)

    # lecture avec vlc
    media_player = vlc.MediaPlayer()
    media_player.audio_set_volume(100)
    media = vlc.Media(file_to_play)
    media_player.set_media(media)
    media_player.play()
    time.sleep(15)
